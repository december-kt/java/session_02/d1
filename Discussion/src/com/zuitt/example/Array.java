package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //[SECTION] Java Collection are a single unit of objects.
    // useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops.
    public static void main(String[] args) {
        //[SECTION] Array
        // In java, arrays are container of values of the same type given a predefined amount of values.
        // java arrays are more rigid, once the size and data type are defined, they can no longer be changed.

        //Syntax: Array Declaration
        // datatype[] identifier = new dataType[numOfElements];
        // "[]" indicates that data type should be able to hold multiple values.
        // "new" keyword is used for non-primitive data types to tell Java to create the said variable.
        // the values of the array is initializes to 0 or null.

        int[] intArray = new int[5];

        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
        // intArray[5] = 100;

        // This will return the memory address of the array
        System.out.println(intArray);

        // This will return the actual array list
        System.out.println(Arrays.toString(intArray));

        //Syntax: Array Declaration with initialization
        //dataType[] identifier = {elementA, elementB, elementC, ...};
        // the compiler automatically specifies the size by counting the number of elements in the array

        String[] names = {"John", "Jane", "Joe"};
        // names[4] = "Joey" -- out of bounds
        System.out.println(Arrays.toString(names));

        // Sample Java Array Method:
        // Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort method: " + Arrays.toString(intArray));

        // Find more here: https://www.geeksforgeeks.org/array-class-in-java/

        //Multidimensional arrays
        //A two-dimensional array then, can be described by two lengths nested within each other, like a matrix.
        // first length is row, second length is column
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom));

        //In Java, the size of the array cannot be modified. If there is a need to add or remove elements, new arrays must be created.

        //[SECTION] ArrayLists are resizable arrays, wherein elements can be added or removed whenever it is needed.
        // Syntax:
        // ArrayList<T> identifier = new ArrayList<String>();
        // "<T>" is used to specify that the list can only have one type of object in a collection.
        // ArrayList cannot hold primitive data types, "java wrapper classes" provide a way to use this types as object.
        // in short, Object version of primitive data types with methods.

        // Declaring an ArrayList
        // ArrayList<int> numbers = new ArrayList<int>(); -- invalid
        // ArrayList<Integer> numbers = new ArrayList<Integer>(); -- valid
        // ArrayList<String> students = new ArrayList<String>();

        // declaring an ArrayList with values
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

        // Add elements
        // arrayListName.add(element);
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        // Access element
        // arrayListName.get(index);
        System.out.println(students.get(0));
        // System.out.println(students.get(5)); -- exception

        // Add an element on a specific index.
        // ArrayListName.add(index, element);
        students.add(0, "Joey");
        System.out.println(students);

        // Updating an element
        // ArrayListName.set
        students.set(0, "George");
        System.out.println(students);

        // Removing a specific element
        //ArrayListName.remove(index)
        students.remove(1);

        // Removing all elements
        students.clear();
        System.out.println(students);

        // Getting the arrayList size
        System.out.println(students.size());

        //[SECTION] Hashmaps
        // most objects in Java are defined and are instantiations of Classes that contain a proper set of properties and methods.
        // There are might be use cases where is this not appropriate, or you may simply want to store a collection of data in key-value pairs
        // in Java "keys" also referred as "fields"
        // wherein the values are accessed by the fields
        // Syntax:
        // HashMap<dataTypeField, dataTypeValue>
        // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<>();

        // Declaring Hashmaps
        // HashMap<String, String> jobPosition = new HashMap<String, String>();

        // Declaring Hashmaps with Initialization
        HashMap<String, String> jobPosition = new HashMap<String, String>() {
            {
                put("Teacher", "John");
                put("Artists", "Jane");
            }
        };

        // Add elements (can also update values)
        // hashMapName.put(<fieldName>,<value>);
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");

        //the latest value will override the previous value whenever the same key is used
        // jobPosition.put("Student", "Jane");

        System.out.println(jobPosition);

        // Accessing elements
        // hashMpName.get("fieldName");
        System.out.println(jobPosition.get("Student"));

        // case-sensitive fields will result to null
        // System.out.println(jobPosition.get("student"));

        // non-existing fields will result to null
        // System.out.println(jobPosition.get("Admin"));

        // Updating the values
        // hashMapName.replace("fieldNameToChange", "newValue");
        jobPosition.replace("Student", "Brandon Smith");

        System.out.println(jobPosition);

        // Remove an element
        // hashMapName.remove("key");
        jobPosition.remove("Dreamer");

        System.out.println(jobPosition);

        // Retrieve HashMap Keys
        // hashMapName.keySet();
        System.out.println(jobPosition.keySet());

        // Retrieve HashMap Values
        // hashMapName.values();
        System.out.println(jobPosition.values());

        // Remove all elements
        // hashMapName.clear();
        jobPosition.clear();
    }

}
